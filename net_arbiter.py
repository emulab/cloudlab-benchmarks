import socket
import argparse
import sys
import time

# Accept one client at a time, keep other clients waiting until they eventually time out 
# or connect.  On first client connect, assign IP to "current_client" variable and 
# return success to client.  If a client connects while "current_client" is set, return 
# wait and wait time to client.  Once client is done, it sends a "release" to server, 
# and if that client matches current_client, then current_client gets unset and the 
# server is ready to serve the next client.

def main():
    # Parse and set up args
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--host", action="store", dest="host", type=str, default="", help="Server IP Address.")
    parser.add_argument("-p", "--port", action="store", dest="port", type=int, default=-1, help="Server Port.")
    args = parser.parse_args()
    
    # Check args
    if (args.host == '') or (args.port < 0):
        print('Invalid Arguments')
        parser.print_help(sys.stderr)
        sys.exit(1)
        
    current_client = ''
    connect_time = time.time()

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((args.host, args.port))
        s.listen()
        while True:
            conn, addr = s.accept()
            status = ''
            with conn:
                print('Connection from:', addr)
                data = conn.recv(1024).decode()
                print('Received operation:', data)
                if (time.time() - connect_time > 90):
                    current_client = ''
                if data == 'request':
                    if current_client == '':
                        print('Setting new current client to', addr[0])
                        current_client = addr[0]
                        connect_time = time.time()
                        status = 'ready'
                    elif current_client == addr[0]:
                        # Shouldn't get here, but just in case...
                        status = 'invalid'
                    else:
                        # Reject
                        status = 'busy'
                        
                elif data == 'release':
                    if current_client == addr[0]:
                        current_client = ''
                        status = 'released'
                    else:
                        status = 'invalid'
                else:
                    status = 'invalid'
                print("Returning status", status)
                conn.sendall(status.encode())
                
##################################################
### Entry point to program, call main function ###
##################################################
if __name__ == "__main__":
    main()
else:
    print('Error, cannot enter main, exiting.')
    sys.exit(1)