#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"

#########################
### Initial Variables ###
#########################
timestamp=$(cat ~/timestamp.out)
run_uuid=$(cat ~/run_uuid.out)
nodeid=$(cat /var/emulab/boot/nodeid)
nodeuuid=$(cat /var/emulab/boot/nodeuuid)
vlan_to_link=$(sudo ip link | grep vlan | awk '{print $2}' | tr '@' ' ' | tr -d ':')
vlan_name=$(echo $vlan_to_link | awk '{print $1}')
net_server=192.168.1.100

####################
### SLANG-probed ###
####################
# Mellanox ConnectX-3 or better experimental NIC is required.
echo -n "Running NIC -> NIC Network Latency Tests - "
date
cd ../SLANG-probed
probed_version=$(./probed | head -1)
probed_interval=1 #1 ms
probed_count=10000

# Run NIC -> NIC latency measurements
probed_status=1
lock_status=2
can_run=1
curr_timeout=0
max_timeout=300

# Send a lock request to the Arbiter
until python3 ../net_client.py -i $net_server -p 65432 -o request
do
    lock_status=$?
    if [ $lock_status -eq 1 ]; then
        can_run=0
        break
    fi
    sleep 5
    let "curr_timeout+=5"
    if [ "$curr_timeout" -gt "$max_timeout" ]; then
        can_run=0
        break
    fi
done

if [ $can_run -eq 1 ]; then
    sudo ./probed -c $net_server -w $probed_interval -n $probed_count -i $vlan_name -o
    probed_status=$?
    # Release the lock on the Arbiter
    python3 ../net_client.py -i $net_server -p 65432 -o release
    if [ $probed_status -eq 0 ]; then
        mv probed_out.csv ~/probed_out.csv
        sed -i '1s/$/,run_uuid,timestamp,nodeid,nodeuuid/' ~/probed_out.csv
        sed -i "2s/$/,$run_uuid,$timestamp,$nodeid,$nodeuuid/" ~/probed_out.csv
    fi
fi
# Generate info file
echo "run_uuid,timestamp,nodeid,nodeuuid,probed_version,probed_count,probed_source_ip,probed_dest_ip,probed_dest_nodeid,probed_interval" > ~/probed_info.csv
echo "$run_uuid,$timestamp,$nodeid,$nodeuuid,$probed_version,$probed_count,$vlan_ip,$net_server,$dest_nodeid,$probed_interval" >> ~/probed_info.csv