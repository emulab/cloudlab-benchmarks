#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"

#########################
### Initial Variables ###
#########################
timestamp=$(cat ~/timestamp.out)
run_uuid=$(cat ~/run_uuid.out)
nodeid=$(cat /var/emulab/boot/nodeid)
nodeuuid=$(cat /var/emulab/boot/nodeuuid)

#########################
### Collect Ping Info ###
#########################
# Define destination host and get the exposed dest_nodeid from server
net_server=192.168.1.100
dest_nodeid=$(curl $net_server:8000/nodeid)
# Check status of previous command.
if [ $? -ne 0 ]; then
    dest_nodeid="NOTFOUND"
fi

# Gather info and set up vars first
ping_version=$(../iputils-ns/ping -V | awk '{print $3}')
ping_count=10000
ping_size=56

echo "run_uuid,timestamp,nodeid,nodeuuid,ping_version,ping_count,ping_source_ip,ping_dest_ip,ping_dest_nodeid,ping_size" > ~/ping_info.csv
echo "$run_uuid,$timestamp,$nodeid,$nodeuuid,$ping_version,$ping_count,$vlan_ip,$net_server,$dest_nodeid,$ping_size" >> ~/ping_info.csv

#####################
### Run Benchmark ###
#####################
ping_status=1
lock_status=2
can_run=1
curr_timeout=0
max_timeout=300

# Send a lock request to the Arbiter
until python3 ../net_client.py -i $net_server -p 65432 -o request
do
    lock_status=$?
    if [ $lock_status -eq 1 ]; then
        can_run=0
        break
    fi
    sleep 5
    let "curr_timeout+=5"
    if [ "$curr_timeout" -gt "$max_timeout" ]; then
        can_run=0
        break
    fi
done

if [ $can_run -eq 1 ]; then
    sudo timeout 30 ../iputils-ns/ping -s $ping_size -f -q -c $ping_count $net_server > ~/temp_ping.out
    ping_status=$?
    # Release the lock on the Arbiter
    python3 ../net_client.py -i $net_server -p 65432 -o release

    # Check status of ping.
    if [ $ping_status -eq 0 ]; then
        pkts_sent=$(grep packets ~/temp_ping.out | awk '{print $1}')
        pkts_received=$(grep packets ~/temp_ping.out | awk '{print $4}')
        pkt_loss=$(grep packets ~/temp_ping.out | awk '{print $6}')
        ping_time=$(grep packets ~/temp_ping.out | awk '{print $10}')

        ping_stats=$(grep rtt ~/temp_ping.out | awk '{print $4}' | tr '/' ' ')
        ping_min=$(echo $ping_stats | awk '{print $1}')
        ping_avg=$(echo $ping_stats | awk '{print $2}')
        ping_max=$(echo $ping_stats | awk '{print $3}')
        ping_mdev=$(echo $ping_stats | awk '{print $4}')

        ping_stats=$(grep rtt ~/temp_ping.out | awk '{print $7}' | tr '/' ' ')
        ping_ipg=$(echo $ping_stats | awk '{print $1}')
        ping_ewma=$(echo $ping_stats | awk '{print $2}')

        ping_units=ms

        echo "run_uuid,timestamp,nodeid,nodeuuid,runtime,packets_sent,packets_received,packet_loss,max,min,mean,stdev,ipg,ewma,units" > ~/ping_results.csv
        echo "$run_uuid,$timestamp,$nodeid,$nodeuuid,$ping_time,$pkts_sent,$pkts_received,$pkt_loss,$ping_max,$ping_min,$ping_avg,$ping_mdev,$ping_ipg,$ping_ewma,$ping_units" >> ~/ping_results.csv
    fi
fi