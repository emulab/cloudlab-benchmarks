#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"/..

#########################
### Initial Variables ###
#########################
timestamp=$(cat ~/timestamp.out)
run_uuid=$(cat ~/run_uuid.out)
nodeid=$(cat /var/emulab/boot/nodeid)
nodeuuid=$(cat /var/emulab/boot/nodeuuid)

###############################
### Environment Information ###
###############################
echo -n "Getting Environment Information - "
date
export DEBIAN_FRONTEND=noninteractive
gcc_ver=$(gcc --version | grep gcc | awk '{print $4}')

# HW info, no PCI bus on ARM means lshw doesn't have as much information
nthreads=$(nproc --all)
total_mem=$(sudo hwinfo --memory | grep "Memory Size" | awk '{print $3 $4}')
arch=$(uname -m)
kernel_release=$(uname -r)
os_release=$(. /etc/os-release; echo "Ubuntu" ${VERSION/*, /})
# Because ARM has to do cpuinfo so differently, hardcode for non x86_64...
nsockets=1
if [ ${arch} == 'x86_64' ]; then
    nsockets=$(cat /proc/cpuinfo | grep "physical id" | sort -n | uniq | wc -l)
    cpu_model=$(lscpu | grep "Model name:" | awk '{print substr($0, index($0, $3))}')
    mem_clock_speed=$(sudo dmidecode --type 17  | grep "Configured Clock Speed" | head -n 1 | awk '{print $4}')
    mem_clock_speed=${mem_clock_speed}MHz
elif [ ${arch} == 'aarch64' ]; then
    cpu_model="ARMv8 (Atlas/A57)"
    mem_clock_speed="Unknown(ARM)"
else
    # Temp placeholder for unknown architecture
    cpu_model="Unknown(Unknown_Arch)"
    mem_clock_speed="Unknown(Unknown_Arch)"
fi

# Is this a Random Execution?  In this script, yes.
random=1
# Semicolon-delimited since variables will have commas.
echo "run_uuid;timestamp;nodeid;nodeuuid;random_indices;random_ops;random_status" > ~/random_info.csv
echo "$run_uuid;$timestamp;$nodeid;$nodeuuid" >> ~/random_info.csv

# Hash
version_hash=$(git rev-parse HEAD)

# Write to file
echo "run_uuid,timestamp,nodeid,nodeuuid,arch,gcc_ver,version_hash,total_mem,mem_clock_speed,nthreads,nsockets,cpu_model,kernel_release,os_release,random" > ~/env_out.csv
echo "$run_uuid,$timestamp,$nodeid,$nodeuuid,$arch,$gcc_ver,$version_hash,$total_mem,$mem_clock_speed,$nthreads,$nsockets,$cpu_model,$kernel_release,$os_release,$random" >> ~/env_out.csv