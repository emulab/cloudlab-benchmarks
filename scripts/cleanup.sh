#!/bin/bash

####################
### Ending Tasks ###
####################
cd ~
# Strip extra whitespace
sed -i 's/  \+/ /g' *.csv
# Push a file to signal run is complete
echo "COMPLETED" > ~/complete/run_complete