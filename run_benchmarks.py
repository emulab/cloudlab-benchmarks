from functools import partial
import os
import sys
import random
import argparse
import subprocess
import platform
import time

####################
### Ping Wrapper ###
####################
def ping():
    cwd = os.path.abspath(os.path.dirname(__file__))
    # Run Ping
    procstring="/bin/bash " + cwd + "/scripts/run_ping.sh"
    print(procstring)
    p=subprocess.call("%s" % procstring, shell=True)
    if p != 0:
        sys.stderr.write("Error in Ping execution.\n")
    return p
    
######################
### STREAM Wrapper ###
######################
def stream(dvfs, socket_num, threading):
    cwd = os.path.abspath(os.path.dirname(__file__))
    # Run STREAM
    procstring="/bin/bash " + cwd + "/scripts/run_memory.sh" + " -n " + socket_num + " -d " + dvfs + " -b stream" + " -t " + threading
    print(procstring)
    p=subprocess.call("%s" % procstring, shell=True)
    if p != 0:
        sys.stderr.write("Error in STREAM execution (" + procstring + ").\n")
    return p
########################
### Membench Wrapper ###
########################
def membench(dvfs, socket_num, argstring):
    cwd = os.path.abspath(os.path.dirname(__file__))
    # Run membench
    procstring="/bin/bash " + cwd + "/scripts/run_memory.sh" + " -n " + socket_num + " -d " + dvfs + " -b membench" + " -a " + str(argstring)
    print(procstring)
    p=subprocess.call("%s" % procstring, shell=True)
    if p != 0:
        sys.stderr.write("Error in membench execution (" + procstring + ").\n")
    return p

#######################
### NPB CPU Wrapper ###
#######################
def npb(benchname, threading, dvfs, socket_num):
    cwd = os.path.abspath(os.path.dirname(__file__))
    # Run NPB
    procstring="/bin/bash " + cwd + "/scripts/run_cpu.sh" + " -n " + socket_num + " -d " + dvfs + " -b " + benchname + " -t " + threading
    print(procstring)
    p=subprocess.call("%s" % procstring, shell=True)
    if p != 0:
        sys.stderr.write("Error in NPB execution (" + procstring + ").\n")
    return p

############################
### SLANG Probed Wrapper ###
############################
def probed():
    cwd = os.path.abspath(os.path.dirname(__file__))
    # Run SLANG Probed
    procstring="/bin/bash " + cwd + "/scripts/run_probed.sh"
    print(procstring)
    p=subprocess.call("%s" % procstring, shell=True)
    if p != 0:
        sys.stderr.write("Error in SLANG Probed execution.\n")
    return p

###################
### Fio Wrapper ###
###################
def fio(benchname, iodepth, device):
    cwd = os.path.abspath(os.path.dirname(__file__))
    # Run fio
    procstring="/bin/bash " + cwd + "/scripts/run_disk.sh" + " -b " + benchname + " -n " + iodepth + " -d " + device
    print(procstring)
    p=subprocess.call("%s" % procstring, shell=True)
    if p != 0:
        sys.stderr.write("Error in fio execution (" + procstring + ").\n")
    return p

#####################
### iPerf Wrapper ###
#####################
def iperf(direction):
    cwd = os.path.abspath(os.path.dirname(__file__))
    # Run iPerf3
    procstring="/bin/bash " + cwd + "/scripts/run_iperf.sh" + " -d " + direction
    print(procstring)
    p=subprocess.call("%s" % procstring, shell=True)
    if p != 0:
        sys.stderr.write("Error in iPerf3 execution (" + procstring + ").\n")
    return p

######################
### Initialization ###
######################
def bench_init():
    cwd = os.path.abspath(os.path.dirname(__file__))
    # Preliminary Operations
    p=subprocess.Popen(["/bin/bash", cwd + "/scripts/init.sh"])
    if p.wait() != 0:
        sys.exit("Error in Preliminary Operations.")
    # Install Packages and Build Benchmarks
    p=subprocess.Popen(["/bin/bash", cwd + "/scripts/build.sh"])
    if p.wait() != 0:
        sys.exit("Error in Benchmark Building.")
    # Gather Environment Info
    p=subprocess.Popen(["/bin/bash", cwd + "/scripts/env_info.sh"])
    if p.wait() != 0:
        sys.exit("Error in Gathering Environment Info.")    
    # Gather Network Info
    p=subprocess.Popen(["/bin/bash", cwd + "/scripts/net_info.sh"])
    if p.wait() != 0:
        sys.exit("Error in Gathering Network Info.")
    # Initialize Disks
    p=subprocess.Popen(["/bin/bash", cwd + "/scripts/disk_init.sh"])
    if p.wait() != 0:
        sys.exit("Error in Disk Initialization.")
###################################
### Construct List of Functions ###
###################################
def build_function_list():
    arch=platform.machine()
    dvfs_omit=subprocess.check_output("lscpu | grep 'Model name:' | grep -o -m 1 -e '6142' -e 'AMD' | head -1", shell=True).strip().decode()
    if (arch == 'aarch64'):
        nsockets=1
    else:
        nsockets=int(subprocess.check_output('cat /proc/cpuinfo | grep "physical id" | sort -u | wc -l', shell=True))
    if_name=subprocess.check_output("sudo ip link | grep vlan | awk '{print $2}' | tr '@' ' ' | tr -d ':' | awk '{print $2}'", shell=True).strip().decode()
    if_hwinfo=subprocess.check_output("sudo lshw -class network -businfo | grep " + if_name + " | awk '{print substr($0, index($0, $4))}'", shell=True).strip().decode()
    print("if_hwinfo: " + if_hwinfo)

    function_list = []
    if (dvfs_omit != '') and (arch == 'x86_64'):
        dvfs_modes=['1']
    elif (arch == 'aarch64'):
        dvfs_modes=['1']
    else:
        dvfs_modes=['1', '0']
    sockets=[]
    for n in range(0, nsockets):
        sockets.append(str(n))
    threading_modes=['ST', 'MT']
    if arch == 'x86_64':
        membench_tests=['rx', 'rl', 'rs', 'rv', 'rvn', 'wl', 'wx', 'ws', 'wsn', 
                        'wv', 'wvn', 'wm', 'orx', 'orl', 'ors', 'orv', 'orvn', 
                        'owl', 'owx', 'ows', 'owsn', 'owv', 'owvn', 'owm']
    else:
        membench_tests=['rl', 'wl', 'wm', 'orl', 'owl', 'owm']
    cpu_tests=['bt', 'cg', 'ep', 'ft', 'is', 'lu', 'mg', 'sp', 'ua']
    disk_tests=['write', 'randwrite', 'read', 'randread']
    iodepths=['4096', '1']
    
    # ping
    function_list.append(partial(ping))
    
    for dvfs in dvfs_modes:
        # STREAM
        for socket_num in sockets:
            for threading in threading_modes:
                function_list.append(partial(stream, dvfs, socket_num, threading))
        # membench
        for socket_num in sockets:
            for memtest in membench_tests:
                function_list.append(partial(membench, dvfs, socket_num, memtest))
        # NPB CPU
        for socket_num in sockets:
            for threading in threading_modes:
                for cputest in cpu_tests:
                    function_list.append(partial(npb, cputest, threading, dvfs, socket_num))
                    
    # SLANG probed
    if "ConnectX" in if_hwinfo:
        print("SLANG probed enabled for NIC " + if_hwinfo)
        function_list.append(partial(probed))
    
    # fio
    home = os.path.expanduser("~")
    try:
        with open(home+"/disk_testdevs.out") as devfile:
            devices = devfile.read().split()
    except Exception as e:
        print("In build_function_list(): " + repr(e) + " - " + str(e))
        print("\tFile: " + str(home+"/disk_testdevs.out"))
        sys.exit(1)
    else:
        devfile.close()
        
    for dev in devices:
        for disktest in disk_tests:
            for iod in iodepths:
                function_list.append(partial(fio, disktest, iod, dev))
        
    # iperf3
    if "NetXtreme" not in if_hwinfo:
        function_list.append(partial(iperf, 'normal'))
        function_list.append(partial(iperf, 'reversed'))
    
    return function_list
        
#####################
### Main function ###
#####################
def main():    
    print("Initializing Benchmark Environment - " + time.strftime("%a %b %d %H:%M:%S %Z %Y"))
    bench_init()
    print("Building Function List - " + time.strftime("%a %b %d %H:%M:%S %Z %Y"))
    ops=build_function_list()
    # Shuffle the indices for random execution.
    tmp_indices=list(enumerate(ops))
    random.shuffle(tmp_indices)
    indices, ops=list(zip(*tmp_indices))
    indices_out=list(indices)
    ops_out=[(str(x.func).strip().split()[1],str(x.args)) for x in ops]
    print("Post-Shuffle Benchmark Order - " + time.strftime("%a %b %d %H:%M:%S %Z %Y"))
    print(indices_out)
    print(ops_out)
    exec_status=[]
    print("Running Benchmarks in Random Order - " + time.strftime("%a %b %d %H:%M:%S %Z %Y"))
    for op in ops:
        ret=op()
        exec_status.append(ret)
    print("Benchmark Execution Complete - " + time.strftime("%a %b %d %H:%M:%S %Z %Y"))
    print(exec_status)
    # Append prior three lists to random info file.
    home = os.path.expanduser("~")
    try:
        with open(home+"/random_info.csv", 'a') as randfile:
            randfile.seek(0, os.SEEK_END)
            randfile.seek(randfile.tell() - 1, os.SEEK_SET)
            randfile.truncate()
            randfile.write(";%s;%s;%s" % (indices_out, ops_out, exec_status))
    except Exception as e:
        print("In main(): " + repr(e) + " - " + str(e))
        print("\tFile: " + str(home+"/random_info.csv"))
        sys.exit(1)
    else:
        randfile.close()
    # Clean Up
    cwd = os.path.abspath(os.path.dirname(__file__))
    p=subprocess.Popen(["/bin/bash", cwd + "/scripts/cleanup.sh"])
    if p.wait() != 0:
        sys.exit("Error in Cleanup.")

##################################################
### Entry point to program, call main function ###
##################################################
if __name__ == "__main__":
    main()
else:
    sys.exit("Error, cannot enter main, exiting.")
