import socket
import argparse
import sys

def main():
    # Parse and set up args
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--host", action="store", dest="host", type=str, default="", help="Destination IP Address.")
    parser.add_argument("-p", "--port", action="store", dest="port", type=int, default=-1, help="Destination Port.")
    parser.add_argument("-o", "--operation", action="store", dest="operation", type=str, default="", help="Operation to Perform.")
    args = parser.parse_args()
    
    # Check args
    if (args.host == '') or (args.port < 0) or (args.operation not in ["request", "release"]):
        print('Invalid Arguments')
        parser.print_help(sys.stderr)
        sys.exit(1)
        
    #HOST = '10.10.1.2'  # The server's hostname or IP address
    #PORT = 65432        # The port used by the server

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((args.host, args.port))
        s.sendall(args.operation.encode())
        data = s.recv(1024).decode()
        print("Received", data)
        if data == "ready" or data == "released":
            sys.exit(0)
        elif data == "busy":
            sys.exit(2)
        else:
            sys.exit(1)

##################################################
### Entry point to program, call main function ###
##################################################
if __name__ == "__main__":
    main()
else:
    print('Error, cannot enter main, exiting.')
    sys.exit(1)